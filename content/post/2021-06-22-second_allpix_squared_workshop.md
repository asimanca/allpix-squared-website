---
title: "2021 06 22 Second_allpix_squared_workshop"
date: 2021-06-22T14:42:28+02:00
draft: false
---
After the very exciting announcement of the release of {{% allpix %}} 2.0., we would like to invite you to the **Second Allpix Squared User Workshop**, taking place wherever you are (virtual) on **August 17-19, 2021**.

This workshop aims to bring together the community of users and developers to discuss new features, software development and numerous applications. It will consist of an introductory talk to the many interesting studies performed with {{% allpix %}}, expert talks and simulation case studies.

We kindly ask you to **register** through the [Indico page of the event](https://indico.cern.ch/event/1043567/), where you can also **submit an abstract** to present your use case of Allpix2. We encourage contributions from all the fields that benefit from this simulation framework.

The poster of the event can be downloaded [here](/pdf/apsq_workshop_2_poster.pdf).


{{< figure src="/img/apsq_workshop_2_poster.png">}}

