---
title: "Patch Release 1.5.2"
date: 2020-09-14T09:22:10+02:00
draft: false
---

We are happy to announce the release of **{{% allpix %}}, version 1.5.2** which contains 42 commits over the previous patch version. It focuses mostly on catching some rare corner cases with invalid input data like pixel pitches of zero.
This patch release contains patches from two new contributors - thank you very much and welcome to the team!

The release is available as Docker image, CVMFS installation, binary tarball and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->

* **Build system:** When checking for installed Git hooks, CMake now considers all and not just one.
* **CI:** Enable all modules for our Docker image build.
* **Release tagging:** The pre-push hook now always prints on success to make sure we are properly tagging the release.
* **Core:** Add multiple checks to catch corner cases with invalid input data:
    * Initialize magnetic field to zero
    * Initialize ConfigManager to `nullptr`
    * Improve variable usage in ModuleManager
    * Avoid division by zero and always provide proper return codes in field tools
    * Initialize members of Pulse objects
    * ConfigManager now checks if config file really is a valid file and not a directory
* **Module CorryvreckanWriter:** Properly initialize MCParticle data members
* **Module DepositionGeant4:** Remove unnecessary dynamic casts when converting track information. The README now properly states that the source energy refers to the kinetic energy only.
* **Module DepositionPointCharge:** Fix order of message dispatch (MCParticles first, DepositedCharges second), properly initialize data members, avoid division by zero in calculating deposition points. `model` is now a required parameter without default value. The `position` parameter now has `source_position` as alias to allow a more coherent syntax among deposition modules.
* **Module GeometryBuilderGeant4:** Properly override parametrization class from Geant4 instead of shadowing it.
* **Module GenericPropagation:** Check return code of deleting animation files. Fix off-by-one error on axis labes for line graphs.
* **Module DatabaseWriter:** Remove unused database index.
* **Module WeightingPotentialReader:** Use the correct pitch in 2D weighting potential plot.
* **Module PulseTransfer:** Improved plotting and correct axis lables stating the current induced.
